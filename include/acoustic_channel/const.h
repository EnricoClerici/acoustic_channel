/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   const.h
 * Author: clerici
 *
 * Created on July 23, 2020, 4:24 PM
 */

#ifndef CONST_H
#define CONST_H

#define NULL_TASK ""


// NULL_STATUS deve essere il minore di tutti 
#define NULL_STATUS (long)0
#define IDLE (long)1            //do nothing
#define LOAD_PARAMS (long)2        //init mission
#define PMMQ (long)3            //pam mantaining quote
#define NAVTOPAM (long)4        //navigation to pam area

#define GTPA (long)5            //gliding to pam area
#define PMPQ (long)6            //pam mission profiler to quote
#define GTSA (long)7            //gliding/navigating to safe area
#define GTRA (long)8            //gliding/navigating to resilience area
#define GOUP (long)9           // emersion
#define QADJ (long)10           //translate to quote
#define GODOWN (long)11           //profiler to quote

#define HOME (long)12         //mantain home direction
#define CHARGE (long)13           //waiting charge of battery
// CHARGE DEVE ESSERE ULTIMO !! maggiore di tutti


#define UCONTROLTOUT 1000 

#define ADJMAXSTEP 100


#define PI               3.1415926535897932384626433832795f
#define DEGTORAD         57.295779513082320876798154814105f

#endif /* CONST_H */

