#ifndef ACOUSTIC_CHANNEL_H
#define ACOUSTIC_CHANNEL_H

#include <iostream>
#include <unistd.h>
#include <string.h>
#include <vector>
#include <time.h>
#include <cmath>

#include <netinet/in.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <netdb.h>
#include <boost/algorithm/string.hpp>
#include <ros/ros.h>
#include <boost/filesystem.hpp>
#include "gt_msgs/NavSts.h"
#include "f300_msgs/Power.h"
#include "f300_msgs/TaskDynPos.h"
#include "f300_msgs/TaskGoUp.h"
#include "f300_msgs/TaskMove.h"
#include "f300_msgs/TaskProfiler.h"
#include "f300_msgs/TaskSurfaceNav.h"
#include "f300_msgs/TaskSurfaceNavCompass.h"
#include "f300_msgs/TaskSurfacePos.h"
#include "f300_msgs/TaskUnderwaterNav.h"
#include "f300_msgs/TaskWait.h"
#include "f300_msgs/TaskGlider.h"
#include "f300_msgs/TaskEvent.h"
#include "f300_msgs/Motors.h"
#include "gt_msgs/NED.h"
#include "gt_msgs/RPY.h"
#include "gt_msgs/GPS.h"
#include "std_msgs/Empty.h"
#include "std_msgs/Bool.h"
#include "f300_msgs/TaskMove.h"
#include "f300_msgs/MoveType.h"
#include "f300_msgs/MoveDirection.h"
#include "f300_msgs/MoveMode.h"
#include "dmac/DMACRaw.h"
#include "dmac/DMACPayload.h"
#include "acoustic_channel/const.h"
#include "gt_lib/async_tcp_client.h"

#include "gt_lib/udp_pair.h"




using namespace std;


class AcousticChannel
{

public:
    AcousticChannel();
    void run();

    long simulator = 0;
    bool disableEmergency = false;
    int auvUdpCommand;
    
    
private:
    
  ros::NodeHandle node_obj;
  ros::Subscriber s_ReceivedMessage;
  ros::Publisher p_SendMessage;

  ros::Subscriber s_updateControl;
  ros::Subscriber s_updateFeedback,s_updateFeedbackMotors;
  ros::Subscriber s_updateFeedbackNav,s_updateGpsNav;  
  ros::Publisher taskDynPosPub,taskGoUpPub,taskMovePub,taskProfilerPub;
  ros::Publisher taskSurfaceNavPub,taskSurfaceNavCompassPub,taskSurfacePosPub,taskUnderwaterNavPub;
  ros::Publisher tskWaitPub,tskGliderPub;
  ros::Publisher KeepAlivePub,TakeControlPub,FreeControlPub,ExitControlPub;

  ros::Publisher AcquisitionPub,StopRecordPub;
  
  std_msgs::Empty KeepAlive,TakeControl,GeneralEmpty;
  std_msgs::Bool Restart;
  
  ros::Timer checkPushedTimer, fbkUpdateTimer, ucWdTimer, reachTimer,checkEmergency;
  
  f300_msgs::TaskDynPos homeDynPos;
  f300_msgs::TaskWait tskWait;
  f300_msgs::TaskGlider tskGlider;

  f300_msgs::TaskSurfacePos homeSurfPos;
  f300_msgs::TaskGoUp tskGoUp;
  f300_msgs::TaskProfiler tskProfiler;
  f300_msgs::Power pow;
  
  f300_msgs::TaskMove tskTraslate;
  f300_msgs::Motors fbkMotors;
   std::string auvStatusStr, taskStr, taskStatusStr, groupId;


    double fbk_depth;
    double fbk_roll;
    double fbk_pitch;
    double fbk_heading;
    double fbk_latitude;
    double fbk_longitude;
    double fbk_battcharge;    

    double fbk_front_horizontal;
    double fbk_front_vertical;
    double fbk_rear_horizontal;
    double fbk_rear_vertical;
    double fbk_thruster;

    std::vector<double> fbk_batt_vec;

    long fbk_bp;
    long fbk_pr;
    double valuetest=0;

    double ref_depth;
    double ref_pitch;
    double LastPMdepth[ADJMAXSTEP];
    double LastPMpitch[ADJMAXSTEP];
    double MissionDepth;

    long LastPMbattpos[ADJMAXSTEP];
    long LastPMpresscam[ADJMAXSTEP];
    ulong adjstep = 0;

    long long MissionElapsed;
    long long last_loop_time;
    long long last_log_time;
    long long SA_time_init;// safe area arriving time
    
    bool PayloadON;
    bool EndMission;

    long current_status;
    long prev_status;
    long next_status;
    long last_started;
    long MissionComplete = 1;
    long long pmmq_time[ADJMAXSTEP];    
    long long pmmq_Tottime,pmmq_Start;
    long long gtpa_Duration,pmpq_Duration,goup_duration,gtsa_Duration;
    bool MissionRunning = false,MissionCompleted = false;
    bool task_sent_run = false;

    float MaxDistance = 1.0;

    int CounterPMPQ;
    bool MissionAborted = false;
    
    std::string MissionActive = "";
    std::string LastMissionActive = "";
    std::string myPath = "/tmp/";
    std::string MyFileMission;

    std::string current_task;    
    std::string executed_task;
    std::string next_task,task_sent;
    std::string txtAuvStatus;
    std::string txtLastTaskEvent;
    
    long long time_init;
    long long time_init_status;

    ros::Time time_begin;    
    std::vector<std::string> task_vector;

    ros::Duration MyTime();
    
    // communication
    void exit_user_control();
    std::string delete_termination(std::string s_msg);
    std::string delete_message(std::string s_msg);

    string set_next_task(std::string task);
    long start_task();

    void updateRestartCbk(const std_msgs::Bool::ConstPtr& StartStatus);
  
    gt::UdpPair *clientUdp;
  
    gt::AsyncTcpClient *client;
  
    std::string clientName, address, port;    
    std::string clientNameUdp, addressUdp, portUdp;    
    
    
	typedef struct
	{
		std::string macAddr;
		int productId;
		unsigned short fwVer;
		std::string netBiosName;
		in_addr ipAddr;
		std::string ifcName;  // client ifc name
		bool wifi; // this is only valid for virnet devices

		int discoveryPort;
		bool valid;

		void clear()
		{
			macAddr.clear();
			productId = 0;
			fwVer = 0;
			netBiosName.clear();
			ipAddr.s_addr = 0;
			ifcName.clear();
			wifi = false;
			discoveryPort = 0;
			valid = false;
		};
	}NetDiscoveryInfo;
        
	typedef struct
	{
		std::string name;			// Network interface name
		sockaddr_in addr;			// Network address of this interface.
		sockaddr_in netmask; 		// Netmask of this interface.
	} NetIfcDesc;
	static std::vector<NetIfcDesc> getNetIfcDescs(std::string ifcName = "");
        
    int initUdpSocket(const sockaddr_in& MyAddr, const in_addr& auvAddr,const int& auvPort);
//void AcousticChannel::establishConnection();
    //helper
    std::vector<std::string> split_string(std::string InputStr, std::string delimiter);
    
    void log_variables();
    void log(std::string str);
    long long get_ms_since_epoch();
    double degrees_to_radians(double angle);
    double radians_to_degrees(double angle);
    
    long GetNextPhase(long current_status);
    std::string GetPhaseFilename(long current_status);
    bool PowerEndPayload(long status);
    bool PowerPayload(long status);
    void AdjustBuoy(ulong step);
    long set_Payload(bool pl);
    bool check_task(std::string filepath);
    double push_average(double sample);
long sizerecv;

f300_msgs::TaskEvent MyTaskEvent;


  void reachTimerCbk(const ros::TimerEvent&);
  void checkEmergencyCbk(const ros::TimerEvent&);

    void updateFeedbackNavCbk(const gt_msgs::NavSts::ConstPtr& Feedback);
    void updateFeedbackMotorsCbk(const f300_msgs::Motors::ConstPtr& Feedback);
    void updateFeedbackGpsCbk(const gt_msgs::GPS::ConstPtr& Gps);

    void updateTaskCbk(const f300_msgs::TaskEvent::ConstPtr& TaskEvent);
    void ReceivedMessageCbk(const dmac::DMACRaw::ConstPtr& InMessage);
    void updateFeedbackCbk(const f300_msgs::Power::ConstPtr& power);

    void Wait(long duration);
    void Goup();
    void Profiler();
    void Traslate();
    void GliderToPam();
    void GliderToSA();
    void NavToPos();
    void NavToHome();
    void MantainHome();
    void PointHome(long duration);

    
};

#endif // ACOUSTIC_CHANNEL_H
