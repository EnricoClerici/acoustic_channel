#include "acoustic_channel/acoustic_channel.h"
#include <cmath>
#include <numeric>

//#define PI                              3.1415926535897932384626433832795
#define MAX_MSG_SIZE                 1000
#define TCP_DISCONNECTION_TIMEOUT     1500    // [msec]
#define NSECS_PER_SEC 1000000000




using namespace std;
using namespace boost::filesystem;
stringstream tmpstr;





ros::Duration AcousticChannel::MyTime()
{
ros::Time time_end = ros::Time::now();
ros::Duration duration = time_end - time_begin;
return duration;
}

int main(int argc, char **argv)
{

	ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug);
	ros::init(argc, argv,"acoustic_channel");
  	AcousticChannel acoustic_channel;

  while (ros::ok()){
		//ros::MultiThreadedSpinner spinner(2);
		//spinner.spin();
		//ros::spinOnce();
                    ros::spin();

        //acoustic_channel.run();
  }
  return 0;
}



// class constructor
AcousticChannel::AcousticChannel()
{
  ros::NodeHandle nodeLocal("~");	
  groupId = ros::this_node::getNamespace();
	boost::erase_all(groupId, "/");
    
    fbk_depth   = 0.0;     //  [dm]
    fbk_roll    = 0.0;     // [deg]
    fbk_pitch   = 0.0;     // [deg]
    fbk_heading = 0.0;     // [deg]

    fbk_bp   = 0;     //  [dm]
    fbk_pr   = 0;     //  [dm}

    ref_depth   = 0.0;     //  [dm]
    ref_pitch   = 0.0;     // [deg]

    PayloadON = false;
    std::string param;
    int SimBattLow = 0;

    /* ros::NodeHandle nodeLocal("~");
   

      if(!nodeLocal.getParam("simbattlow", SimBattLow)) 
      { 
        
        ROS_WARN("param int =  %d", SimBattLow);
      }

    if(!nodeLocal.getParam("simbattlow", param)) 
      { 
        
        ROS_WARN("param =  %s", param.str().c_str());
      }
    */
          ROS_INFO("Acoustic_channel_started");

    s_updateGpsNav =  node_obj.subscribe("sensor/gps", 1, &AcousticChannel::updateFeedbackGpsCbk, this);
    s_updateFeedback = node_obj.subscribe("sensor/power", 1, &AcousticChannel::updateFeedbackCbk, this);
    s_updateFeedbackNav =  node_obj.subscribe("feedback/navigation_states", 1, &AcousticChannel::updateFeedbackNavCbk, this);
    s_updateFeedbackMotors =  node_obj.subscribe("feedback/motors", 1, &AcousticChannel::updateFeedbackMotorsCbk, this);

    s_ReceivedMessage = node_obj.subscribe("dmac_node/raw", 1, &AcousticChannel::ReceivedMessageCbk, this);
    p_SendMessage = node_obj.advertise<dmac::DMACPayload>("dmac_node/send", 1);


    nodeLocal.getParam("missionpath", myPath);
    char file[50];
    sprintf(file,"LOG_%lld.log",get_ms_since_epoch());

    sprintf(file,"//tmp//acoustic_channel.log");

    freopen(file,"a+",stdout);

      ROS_INFO("Acoustic_channel_started1");

    time_begin = ros::Time::now();
  //client = new gt::AsyncTcpClient(clientName, address, port);
  
  clientNameUdp = groupId + " acoustic_channel";
  port = "61501";
  portUdp = "51501";
  addressUdp = "172.22.0.132";
  if(!nodeLocal.getParam("auv_ip", addressUdp)) { ROS_ERROR("No backseat ip"); ros::shutdown(); }

  clientUdp = new gt::UdpPair(clientNameUdp,port, addressUdp, portUdp);

}

void AcousticChannel::reachTimerCbk(const ros::TimerEvent&)
{
    log_variables();
    KeepAlivePub.publish(KeepAlive);

}


void AcousticChannel::updateFeedbackNavCbk(const gt_msgs::NavSts::ConstPtr& Feedback)
{

      fbk_depth =   Feedback->position.linear.depth;
      fbk_roll =   Feedback->position.angular.roll;
      fbk_pitch =   Feedback->position.angular.pitch;
      fbk_heading =   Feedback->position.angular.yaw;



}
void AcousticChannel::updateFeedbackMotorsCbk(const f300_msgs::Motors::ConstPtr& Feedback)
{

      fbk_front_horizontal =   Feedback->front_horizontal;
      fbk_front_vertical =   Feedback->front_vertical;
      fbk_rear_horizontal =   Feedback->rear_horizontal;
      fbk_rear_vertical =   Feedback->rear_vertical;
      fbk_thruster =   Feedback->thruster;



}
void AcousticChannel::updateFeedbackGpsCbk(const gt_msgs::GPS::ConstPtr& Gps)
{

    fbk_latitude = Gps->latitude;
    fbk_longitude = Gps->longitude;
}


void AcousticChannel::updateFeedbackCbk(const f300_msgs::Power::ConstPtr& power)
{
      fbk_battcharge = push_average(power->battery);
      //ROS_WARN("CHARGE: %lf ",fbk_battcharge); TBD



}
void AcousticChannel::updateRestartCbk(const std_msgs::Bool::ConstPtr& StartStatus)
{
          current_status = IDLE;
          MissionCompleted = false;
}


void AcousticChannel::ReceivedMessageCbk(const dmac::DMACRaw::ConstPtr& InMessage)
{
  
  ros::Time timestamp;
  std::vector<std::string> s_v;

  timestamp = ros::Time::now();

  ROS_WARN("=================> Received Message %s time: %i,%i timestamp %i,%i",InMessage->command.c_str(),timestamp.sec,timestamp.nsec,InMessage->stamp.sec,InMessage->stamp.nsec);

  timestamp = InMessage->stamp;
  
  ROS_WARN("AcousticChannel InMessage=%s",InMessage->command.c_str());

  s_v = split_string(InMessage->command, ",");

  if(s_v.size() > 0)
  {
    //if(s_v.size() == 1)
      s_v.at(0) = delete_termination(s_v.at(0));
    if(s_v.at(0) == "$ABORT")
    {
        ROS_WARN("AcousticChannel InMessage=%s send $ABORT   to AUV",s_v.at(0).c_str());
        clientUdp->connect();
        clientUdp->send_to_endpoint("$ABORT\r\n");
        clientUdp->disconnect();
        dmac::DMACPayload ToSend;
        ToSend.msg_id = 2;
        ToSend.payload = "$ACKABORT";
        ToSend.ack = false;
        ToSend.force = true;
        ToSend.bitrate = 19200;
        ToSend.destination_address = 3;
        ToSend.source_address = 8;
        ToSend.source_name = "X300_07";
        ToSend.timestamp = InMessage->stamp.sec;
        ToSend.relative_velocity = 1530;
        p_SendMessage.publish(ToSend);

    }
    if(s_v.at(0) == "$ATTITUDE")
    {
        ROS_WARN("AcousticChannel InMessage=%s",InMessage->command.c_str());

    }
    if(s_v.at(0) == "$DEPTH")
    {
        ROS_WARN("AcousticChannel InMessage=%s",InMessage->command.c_str());

    }      
  }

}





void AcousticChannel::exit_user_control()
{
    FreeControlPub.publish(TakeControl);
    ROS_WARN("######################################## Exit requested to UserControl");
    ExitControlPub.publish(TakeControl);
}






double AcousticChannel::push_average(double sample)
{
  double fbk_battcharge_avg;

  fbk_batt_vec.emplace(fbk_batt_vec.begin(),sample);
  if(fbk_batt_vec.size() >= 10)
    fbk_batt_vec.resize(10);

  fbk_battcharge_avg =  accumulate( fbk_batt_vec.begin(), fbk_batt_vec.end(), 0.0)/fbk_batt_vec.size();

 // cout << "The vector elements are: ";
 //   for (auto it = fbk_batt_vec.begin(); it != fbk_batt_vec.end(); ++it)
 //       cout << *it << " ";
 // cout << "Average " << fbk_battcharge_avg << " "<< endl;

  return fbk_battcharge_avg;
}



std::string AcousticChannel::delete_termination(std::string s_msg)
{
    int r_index = s_msg.find("\r");
    int n_index = s_msg.find("\n");

    std::string s;

    if(r_index < n_index && r_index > -1)
    {
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(n_index+1,s_msg.length());
    }
    else if (n_index < r_index && n_index > -1)
    {
        s = s_msg.substr(0,n_index);
        s_msg = s_msg.substr(r_index+1,s_msg.length());
    }
    else if (n_index == -1)
    {
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(r_index+1,s_msg.length());
    }
    else if (r_index == -1)
    {
        s = s_msg.substr(0,n_index);
        s_msg = s_msg.substr(n_index+1,s_msg.length());
    }
    else
    {
        log("No termination !!!!");
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(r_index+1,s_msg.length());
    }

    s_msg.empty();
    return s;
}

std::string AcousticChannel::delete_message(std::string s_msg)
{
    int r_index = s_msg.find("\r");
    int n_index = s_msg.find("\n");

    std::string s;

    if(r_index < n_index && r_index > -1)
    {
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(n_index+1,s_msg.length());
    }
    else if (n_index < r_index && n_index > -1)
    {
        s = s_msg.substr(0,n_index);
        s_msg = s_msg.substr(r_index+1,s_msg.length());
    }
    else if (n_index == -1)
    {
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(r_index+1,s_msg.length());
    }
    else if (r_index == -1)
    {
        s = s_msg.substr(0,n_index);
        s_msg = s_msg.substr(n_index+1,s_msg.length());
    }
    else
    {
        log("No termination !!!!");
        s = s_msg.substr(0,r_index);
        s_msg = s_msg.substr(0,s_msg.length());
    }

    return s_msg;
}

std::vector<string> AcousticChannel::split_string(std::string InputStr, std::string delimiter)
{
    std::vector<std::string> vector_str;
    std::size_t pos = 0;
    std::string token;
    while ((pos = InputStr.find(delimiter)) != std::string::npos)
    {
        token = InputStr.substr(0, pos);
        vector_str.push_back(token);
        InputStr.erase(0, pos + delimiter.length());
    }

    vector_str.push_back(InputStr);

    return vector_str;
}

void AcousticChannel::log_variables()
{
    char fbk_msg[MAX_MSG_SIZE];
    char sts_msg[MAX_MSG_SIZE];
    sprintf(fbk_msg,"ST:=%5.1f %5.1f %5.1f %5.1f %5.2f %f %f %3ld %3ld %lf",fbk_depth,fbk_roll,fbk_pitch,fbk_heading,fbk_battcharge,fbk_latitude,fbk_longitude,fbk_bp,fbk_pr,MaxDistance);
    log(std::string(fbk_msg));
    

}

void AcousticChannel::log(std::string str)
{
    ROS_WARN("%s",str.c_str());
  //  char ms[20];
  //  sprintf(ms,"%lld",get_ms_since_epoch());
  //  std::cout << ms << " " << str << endl;
}


long long AcousticChannel::get_ms_since_epoch()
{
    timeval tv;
    gettimeofday(&tv,NULL);

    double ms = (((tv.tv_sec) * 1000.0) + ((tv.tv_usec) / 1000.0) + 0.5);
    long long milli = (long long)round(ms);
    return milli;
}

double AcousticChannel::degrees_to_radians(double angle)
{
    return ((angle * PI) / 180.0);
}

double AcousticChannel::radians_to_degrees(double angle)
{
    return ((angle * 180.0) / PI);
}

/*
void AcousticChannel::establishConnection()
{
	log("AcousticChannel::establishConnection");

	AcousticChannel::NetDiscoveryInfo discoveryInfo = AcousticChannel::getDiscoveryInfo(mDaqDeviceDescriptor.uniqueId);

	if(!discoveryInfo.valid)
		discoveryInfo = mNetDiscoveryInfo;  // use the last discovery info if discovery info for the specified device is not available anymore

	if(discoveryInfo.valid)
	{
		if(AcousticChannel::isNetIfcAvaiable(discoveryInfo.ifcName))
		{
			AcousticChannel::NetIfcDesc ifcDesc = AcousticChannel::getNetIfcDescs(discoveryInfo.ifcName)[0];

			int err = initUdpSocket(ifcDesc, discoveryInfo);

			if(err)
			{
				closeSockets();
				exit(err);
			}
		}
		else
          exit(-1);

	}
	else
          exit(-1);
}
*/
std::vector<AcousticChannel::NetIfcDesc> AcousticChannel::getNetIfcDescs(std::string ifcName)
{
	std::vector<NetIfcDesc> netIfcDescs;
	ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) != -1)
	{
		for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next)
		{
			if (ifa->ifa_addr == NULL)
				continue;

			if ((ifa->ifa_addr->sa_family == AF_INET) && !(ifa->ifa_flags & IFF_LOOPBACK))
			{
				NetIfcDesc desc;

				desc.name = ifa->ifa_name;
				desc.addr = *((sockaddr_in*) ifa->ifa_addr);
				desc.netmask = *((sockaddr_in*) ifa->ifa_netmask);
				//desc.broadaddr = *((sockaddr_in*) ifa->ifa_ifu.ifu_broadaddr);

				if(ifcName.empty() || (!ifcName.empty() && ifcName == desc.name))
					netIfcDescs.push_back(desc);
			}
		}

		freeifaddrs(ifaddr);
	}
	else
	{
		ROS_ERROR("unable to detect network interfaces, getifaddrs() error: %s",strerror(errno));
	}

	if(!ifcName.empty() && netIfcDescs.empty())
		exit(-1);

	return netIfcDescs;
}


int AcousticChannel::initUdpSocket(const sockaddr_in& MyAddr, const in_addr& auvAddr,const int& auvPort)
{
std::string Outstr;  
	ROS_INFO("AcousticChannel::initUdpSocket");

	int err = -1; // don't change

	auvUdpCommand = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if(auvUdpCommand != -1)
	{
		// bind to network interface (any port)
		if(bind(auvUdpCommand, (sockaddr*) &MyAddr, sizeof(sockaddr)) == 0)
		{
			sockaddr_in targetAddr = {0};
			targetAddr.sin_family = AF_INET;
			targetAddr.sin_addr = auvAddr;
			targetAddr.sin_port = htons(auvPort);

			// bind to auv device so we can use send() rather than sendto()
			if(::connect(auvUdpCommand, (sockaddr*) &targetAddr, (socklen_t) sizeof(sockaddr)) == 0)
			{
				err = 0;
			}
			else
            {
				ROS_ERROR("UDP socket connection failed, connect() error: %s" ,strerror(errno));
            }
		}
		else
        {
			ROS_ERROR("UDP socket bind failed, bind() error: %s" ,strerror(errno));
        }

		if(err)
		{
			close(auvUdpCommand);
			auvUdpCommand = -1;
		}
	}
	else
    {
		ROS_ERROR("UDP socket creation failed, socket() error: %s",strerror(errno));
    }

	return err;
}
